#include "stdafx.h"
#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;
using std::cin;

typedef int Type;

class Point {};

class Area{
	Type ** p;
	int N,M;
public:
	Area(const int _N, const int _M){
		N = _N;
		M = _M;
		p = new Type* [N];
		//��������� ������ � ����
		for (int i = 0; i < N; i++)
			p[i] = new Type[M];
	}
	
	void fillTest() const
	{
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				*(*(p + i) + j) = i + 10 *j;
	}
	void fill2(const int Flow, const int Wall1 = 0, const int Wall2 = 0) const
	{
		try
		{
			for (int i = 0; i < N; i++)
			{		
				for (int j = 0; j < M; j++)
				{
					if(i == 0)
					{
						*(*(p + i) + j) = Wall1;
					} 
					else if(i == N - 1)
					{
						*(*(p + i) + j) = Wall2;
					}
					else
					{
						*(*(p + i) + j) = Flow;
					}
				}
			}
		}
		catch(...)
		{
			cout << "Except\n";
		}
	}
	void showArr() const {
		for (int j = 0; j < M; j++){
			for (int i = 0; i < N; i++)
				cout << *(*(p + i) + j) << "\t";
		cout << "\n";
		}
	}
	~Area(){
		//������������ ������
		for (int i = 0; i < N; i++)
			delete [] p[i];
		delete [] p;
	}

	Type get(const int i,const int j) const
	{
		try
		{
			if(i < 0 || j < 0 || i >= this->N || j >= this->M)
			{
				throw "Out of range from limits array in function get(i,j)\n ";
			}
			else
			{
				return *(*(p + i) + j); 
			}
		}
		catch(char * ex)
		{
			cout << ex;
		}
	}
	void set(const int i,const int j, const Type NewValue)
	{
		try
		{
			if(i < 0 || j < 0 || i >= this->N || j >= this->M)
			{
				throw "Out of range from limits array in function get(i,j)\n ";
			}
			else
			{
				*(*(p + i) + j) = NewValue; 
			}
		}
		catch(char * ex)
		{
			cout << ex;
		}	
	}
	void razdelitel() const {
		cout << "++++++++++++++++++++++++\n\n";
		cout << "++++++++++++++++++++++++\n";
	}
	void ShowElemArea(int i, int j) const {
		cout << get(i,j) << "\n";
	}
};

class SolutionArea{
public:
	SolutionArea(int N, int M)
	{
		Area arr(N,M), tmp(N,M);//��������� ������ ��� ������ N M
		
		arr.fill2(3); //���������� �������� = 3 �� ������ ���� �� ���������
		arr.showArr();
		arr.razdelitel();
		tmp.fillTest();
		tmp.showArr();
		arr.razdelitel();
		
		
		
		tmp.ShowElemArea(-1,5);
		tmp.ShowElemArea(2,5);
		tmp.set(2,5,1000);
		tmp.ShowElemArea(2,5);
		
		Type epsilon = 1, epsilon0 = 0.001;

		while (epsilon > epsilon0)
		{
			for (int i = 0; i < N; i++)
				for (int j = 0; j < M; j++)

		}



//������������ �������
// arr[i,j] = { tmp[i + 1,j] + tmp[i - 1,j] + b*b * ( tmp[i,j + 1] + tmp[i,j - 1] ) } * (0.5 / (1 + b * b))
// b = 0.1
	}
	void CallMethod(){
		
	
	}
};

class SolutionMethod{
public:
	virtual Type Method() = 0;
};

class Boundary : public SolutionMethod 
{
public:
	Type Method()
	{
		cout << "Boundary\n";
		return 0;
	}
};
class Inner : public SolutionMethod 
{
public:
	Type Method()
	{
		cout << "Inner\n";
		return 0;
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	SolutionArea tube(7,10);

	SolutionMethod * psmB = new Boundary;
	SolutionMethod * psmI = new Inner;

	psmB->Method();
	psmI->Method();

	_getch();
}

